package com.example.tictactoegame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Point
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class Game(context: Context?, attrs: AttributeSet?) : View(context, attrs) {
    private var mHeight = 0
    private var mWidth = 0
    private var squareSize = 0.0f
    private var field = Array(10) { Array(10) { 0 } }
    private var currentPlayer = false // 0 - first  1 - second
    private var gameOn = true
    private var winLines = ArrayList<Pair<Point, Point>>()

    private var callback: GameCallback? = null

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            squareSize = if(mWidth < mHeight) (mWidth / 10).toFloat() else (mHeight / 10).toFloat()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            drawField(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(gameOn){
            when(event?.actionMasked) {
                MotionEvent.ACTION_DOWN -> return true
                MotionEvent.ACTION_UP -> {
                    clickField((event.x / squareSize).toInt(), (event.y / squareSize).toInt())
                    return true
                }
            }
        }
        return super.onTouchEvent(event)
    }

    fun setCallback (callback: GameCallback) {
        this.callback = callback
        callback.setPLayer(currentPlayer)
    }

    fun newGame() {
        field.forEach { it.fill(0) }
        gameOn = true
        winLines.clear()
        invalidate()
        currentPlayer = true
    }

    private fun drawField(canvas: Canvas) {
        canvas.drawColor(Color.parseColor("#979797"))

        val p = Paint()
        p.style = Paint.Style.STROKE

        for( i in 0 until 10 ) {
            for( j in 0 until 10 ) {
                p.strokeWidth = 2.0f
                p.color = Color.BLACK
                canvas.drawRect(squareSize * i, squareSize * j, squareSize * (i + 1), squareSize * (j + 1), p)
                when(field[i][j]) {
                    1 -> {
                        p.strokeWidth = 5.0f
                        p.color = Color.BLUE
                        val interval = squareSize / 10
                        canvas.drawLine(squareSize * i + interval, squareSize * j + interval, squareSize * (i + 1) - interval , squareSize * (j + 1) - interval, p)
                        canvas.drawLine(squareSize * (i + 1) - interval, squareSize * j + interval, squareSize * i + interval, squareSize * (j + 1) - interval, p)
                    }
                    2 -> {
                        p.strokeWidth = 5.0f
                        p.color = Color.RED
                        canvas.drawCircle(squareSize * i + squareSize * 0.5f, squareSize * j + squareSize * 0.5f, (squareSize / 2) - (squareSize / 10), p)

                    }
                }
            }
        }

        p.color = Color.RED
        p.strokeWidth = 9.0f

        for ( line in winLines ) {
            canvas.drawLine(line.first.x * squareSize + squareSize * 0.5f
                , line.first.y * squareSize + squareSize * 0.5f
                , line.second.x * squareSize + squareSize * 0.5f
                , line.second.y * squareSize + squareSize * 0.5f, p)
        }
    }

    private fun clickField(x: Int, y: Int) {
        if (field[x][y] == 0) {
            field[x][y] = if(currentPlayer) 1 else 2
            checkWinFor(x,y)
            if(gameOn) {
                currentPlayer = !currentPlayer
                callback?.setPLayer(currentPlayer)
            }
            invalidate()
        }
    }

    private fun checkWinFor(x: Int, y: Int) {
        val current = field[x][y]
        var left = 0
        var topLeft = 0
        var top = 0
        var topRight = 0
        var right = 0
        var bottomRight = 0
        var bottom = 0
        var bottomLeft = 0

        for (i in (x - 1) downTo 0) if(field[i][y] == current) left++ else break
        for (i in (x + 1)..9) if(field[i][y] == current) right++ else break
        for (j in (y - 1) downTo 0) if(field[x][j] == current) top++ else break
        for (j in (y + 1)..9) if(field[x][j] == current) bottom++ else break

        if((x - 1) > (y - 1)) for(i in 1 until y) if(field[x - i][y - i] == current) topLeft++ else break
        else for(i in 1 until x) if(field[x - i][y - i] == current) topLeft++ else break

        if((9 - x) > (y - 1)) for(i in 1 until y) if(field[x + i][ y - i] == current) topRight++ else break
        else for(i in 1..(9 - x)) if(field[x + i][ y - i] == current) topRight++ else break

        if((9 - x) > (9 - y)) for(i in 1..(9 - y)) if(field[x + i][y + i] == current) bottomRight++ else break
        else for(i in 1..(9 - x)) if(field[x + i][y + i] == current) bottomRight++ else break

        if((x - 1) > (9 - y)) for(i in 1..(9 - y)) if(field[x - i][y + i] == current) bottomLeft++ else break
        else for(i in 1 until x) if(field[x - i][y + i] == current) bottomLeft++ else break

        if(left + right >= 4) win(x - left, y, x + right, y)
        if(topLeft + bottomRight >= 4) win(x - topLeft, y - topLeft, x + bottomRight, y + bottomRight)
        if(top + bottom >= 4) win(x, y - top, x, y + bottom)
        if(topRight + bottomLeft >= 4) win(x - bottomLeft, y + bottomLeft, x + topRight, y - topRight)
    }

    private fun win(fromX: Int, fromY: Int, toX: Int, toY: Int) {
        if(gameOn) callback?.onWin(currentPlayer)
        gameOn = false
        winLines.add(Pair(Point(fromX, fromY), Point(toX, toY)))
    }

    interface GameCallback {
        fun setPLayer(first: Boolean)
        fun onWin(winner: Boolean)
    }

}
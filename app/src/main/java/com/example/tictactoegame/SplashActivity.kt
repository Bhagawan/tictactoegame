package com.example.tictactoegame

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.tictactoegame.util.MyWebChromeClient
import com.example.tictactoegame.util.ServerClient
import com.example.tictactoegame.util.SplashResponse
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity(){
    private lateinit var mWebView: WebView
    private var requestInProcess = false
    lateinit var target : Target

    override fun onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack()
            return
        } else finish()
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mWebView = findViewById(R.id.webView_splash)
        initialiseWebView()
        if (savedInstanceState == null) requestSplash()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    fun switchToMain() {
        val i = Intent(applicationContext, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(i)
        finish()
    }

    fun showError(error: String?) = Toast.makeText(this, error, Toast.LENGTH_SHORT).show()

    fun showSplash(url: String?) {
        mWebView = findViewById(R.id.webView_splash)
        mWebView.loadUrl(url!!)
    }

    fun showLogo(logo: Bitmap?) {
        mWebView = findViewById(R.id.webView_splash)
        val logoLayout : ConstraintLayout = findViewById(R.id.layout_splash_logo)
        val imageLogo : ImageView = findViewById(R.id.imageView_splash_logo)
        imageLogo.setImageBitmap(logo)
        mWebView.visibility = View.GONE
        logoLayout.visibility = View.VISIBLE
    }

    fun hideLogo() {
        mWebView = findViewById(R.id.webView_splash)
        val logoLayout = findViewById<ConstraintLayout>(R.id.layout_splash_logo)
        mWebView.visibility = View.VISIBLE
        logoLayout.visibility = View.GONE
    }

    private fun requestSplash() {
        val call : Call<SplashResponse> = ServerClient.create().getSplash(Locale.getDefault().language)
        showLogo()
        call.enqueue(object : Callback<SplashResponse> {
            override fun onResponse(
                call: Call<SplashResponse>,
                response: Response<SplashResponse>
            ) {
                if (requestInProcess) {
                    requestInProcess = false
                    hideLogo()
                }
                if (response.isSuccessful && response.body() != null) {
                    val s: String = response.body()!!.url
                    if (s == "no") switchToMain()
                    else showSplash("https://$s")
                } else {
                    showError("Server error 2")
                    switchToMain()
                }
            }

            override fun onFailure(call: Call<SplashResponse>, t: Throwable) {
                showError("Server error 3")
                switchToMain()
            }
        })
    }

    private fun showLogo() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                if (requestInProcess) showLogo(bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        requestInProcess = true
        Picasso.get().load("http://195.201.125.8/HealthAdviserApp/logo.png").into(target)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initialiseWebView() {
        val frame = findViewById<FrameLayout>(R.id.webView_customView)
        mWebView.webViewClient = WebViewClient()
        mWebView.webChromeClient = MyWebChromeClient(this, mWebView, frame)
        val webSettings = mWebView.settings
        mWebView.scrollBarStyle = View.SCROLLBARS_OUTSIDE_OVERLAY
        val cookie = CookieManager.getInstance()
        cookie.setAcceptCookie(true)
        mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.allowContentAccess = true
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.allowUniversalAccessFromFileURLs = true
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.allowFileAccess = true
        webSettings.domStorageEnabled = true
        webSettings.databaseEnabled = true
        webSettings.useWideViewPort = true
        webSettings.supportZoom()
        webSettings.setAppCacheEnabled(true)
        webSettings.userAgentString = webSettings.userAgentString.replace("; wv", "")
    }
}

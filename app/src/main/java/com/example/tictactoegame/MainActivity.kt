package com.example.tictactoegame

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import android.view.WindowInsets
import android.view.WindowInsetsController
import androidx.appcompat.app.AppCompatActivity
import com.example.tictactoegame.databinding.ActivityMainBinding
import com.example.tictactoegame.game.Game
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var target: Target
    private var scoreOne = 0
    private var scoreTwo = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        fullscreen()
        setBackground()
        updateScore()

        binding.game.setCallback( object: Game.GameCallback {
            override fun setPLayer(first: Boolean) {
                if (first) {
                    val playerOne = SpannableString(getString(R.string.string_player_one))
                    playerOne.setSpan(UnderlineSpan(), 0,playerOne.length, 0)
                    binding.textViewGamePlayerOne.text = playerOne
                    binding.textViewGamePlayerTwo.text = getString(R.string.string_player_two)
                } else {
                    val playerTwo = SpannableString(getString(R.string.string_player_two))
                    playerTwo.setSpan(UnderlineSpan(), 0,playerTwo.length, 0)
                    binding.textViewGamePlayerOne.text = getString(R.string.string_player_one)
                    binding.textViewGamePlayerTwo.text = playerTwo
                }
            }

            override fun onWin(winner: Boolean) {
                if(winner) scoreOne++ else scoreTwo++
                updateScore()
            }
        })

        binding.btnNewGame.setOnClickListener {
            scoreOne = 0
            scoreTwo = 0
            updateScore()
            binding.game.newGame()
        }

        binding.btnNewScore.setOnClickListener { binding.game.newGame() }

    }

    private fun fullscreen() {
        if (Build.VERSION.SDK_INT >= 30) {
            binding.root.windowInsetsController?.hide(WindowInsets.Type.statusBars() or WindowInsets.Type.navigationBars())
            binding.root.windowInsetsController?.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        } else {
            binding.root.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LOW_PROFILE or
                        View.SYSTEM_UI_FLAG_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }

    private fun setBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                binding.layoutMain.background = BitmapDrawable(resources, bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/TicTacToeGame/back.png").into(target)
    }

    private fun updateScore() {
        binding.textScoreOne.text = scoreOne.toString()
        binding.textScoreTwo.text = scoreTwo.toString()
    }

}
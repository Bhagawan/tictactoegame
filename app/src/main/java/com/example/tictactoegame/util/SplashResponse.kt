package com.example.tictactoegame.util

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)


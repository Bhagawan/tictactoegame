package com.example.tictactoegame.util

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.FrameLayout
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MyWebChromeClient(
    private val activity: AppCompatActivity,
    private val webView: WebView,
    private val frameLayout: FrameLayout
) : WebChromeClient() {
    private var mCallback: ValueCallback<Array<Uri>?>? = null
    private var mCustomView: View? = null
    private var mOriginalSystemUiVisibility = 0
    private var mOriginalOrientation = 0
    private var mCustomViewCallback: CustomViewCallback? = null
    private var mCameraPhotoPath: String? = null

    private var mLauncher: ActivityResultLauncher<Intent> = activity.registerForActivityResult(
        StartActivityForResult()
    ) { result: ActivityResult? ->
        if (result != null) {
            val dataString = result.data!!.dataString
            if (dataString != null && mCallback != null) {
                val uri =
                    arrayOf(Uri.parse(dataString))
                mCallback!!.onReceiveValue(uri)
            }
        }
    }

    override fun getDefaultVideoPoster(): Bitmap? {
        return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888)
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = activity.baseContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
    }

    override fun onShowFileChooser(
        webView: WebView?,
        filePathCallback: ValueCallback<Array<Uri>?>,
        fileChooserParams: FileChooserParams?
    ): Boolean {
        if (mCallback != null) {
            filePathCallback.onReceiveValue(null)
        }
        mCallback = filePathCallback
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(activity.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
                takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                mCameraPhotoPath = "file:" + photoFile.absolutePath
                takePictureIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photoFile)
                )
            }
        }
        val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
        contentSelectionIntent.type = "image/*"
        val intentArray: Array<Intent?> = arrayOf(takePictureIntent) ?: arrayOfNulls(0)
        val chooserIntent = Intent(Intent.ACTION_CHOOSER)
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        mLauncher.launch(chooserIntent)
        return true
    }

    override fun onHideCustomView() {
        mCustomView = null
        webView.visibility = View.VISIBLE
        frameLayout.visibility = View.GONE
        mCustomViewCallback!!.onCustomViewHidden()
        mCustomViewCallback = null
        activity.window.decorView.systemUiVisibility = mOriginalSystemUiVisibility
        activity.requestedOrientation = mOriginalOrientation
    }

    override fun onShowCustomView(
        view: View?,
        requestedOrientation: Int,
        callback: CustomViewCallback?
    ) {
        onShowCustomView(view, callback)
    }

    override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
        if (mCustomView != null) {
            onHideCustomView()
            return
        }
        webView.visibility = View.GONE
        mCustomView = view
        mOriginalSystemUiVisibility = activity.window.decorView.systemUiVisibility
        mOriginalOrientation = activity.requestedOrientation
        mCustomViewCallback = callback
        frameLayout.addView(
            mCustomView,
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
            )
        )
        frameLayout.visibility = View.VISIBLE
        WindowInsetsControllerCompat(activity.window, activity.window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(activity.window, false)
    }
}
